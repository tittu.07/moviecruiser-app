import { AppPage } from './app.po';
import { browser, by, element } from '../../node_modules/protractor';
import { async } from '../../node_modules/rxjs/internal/scheduler/async';
import { serializePath } from '../../node_modules/@angular/router/src/url_tree';
import { By } from '../../node_modules/@angular/platform-browser';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();    
    expect(browser.getTitle()).toEqual('MovieCruiserApp')
  });

  it('should route to register page', () => {
    browser.element(by.css('.register')).click();
     expect(browser.getCurrentUrl()).toContain('/register')
  });

  it('should register a new user', () => {
    browser.element(by.css('.reg-fname')).sendKeys('testuser');
    browser.element(by.css('.reg-lname')).sendKeys('testuser');
    browser.element(by.css('.reg-uname')).sendKeys('testuser');
    browser.element(by.css('.reg-pwd')).sendKeys('testuser');
    browser.element(by.css('.reg-btn')).click();
     expect(browser.getCurrentUrl()).toContain('')
  });

  it('should able to login and navigate to popular movies', () => {
    browser.element(by.css('.lg-usr')).sendKeys('testuser');
    browser.element(by.css('.lg-pwd')).sendKeys('testuser');    
    browser.element(by.css('.lg-btn')).click();
     expect(browser.getCurrentUrl()).toContain('/popular')
  });

  it('should able to search movies', () => {
    browser.element(by.css('.txt-search')).sendKeys('spider');     
    browser.element(by.css('.btn-search')).click();
     expect(browser.getCurrentUrl()).toContain('/search/spider')
  });

  it('should able to add movie to watchlist',async() => {
    browser.driver.manage().window().maximize();
    browser.driver.sleep(1000);
    const searchItem = element.all(by.css('.movie-thumbnail'));
    expect(searchItem.count()).toBe(20);
    searchItem.get(0).click();
    browser.element(by.css('.btn-add')).click();
    browser.driver.sleep(5000);
  });

 
  it('should load wathlist page',async() => {    
    browser.element(by.css('.tab-wat-lst')).click();
    const items = element.all(by.css('.wat-lst-mai-container'));
    expect(items.count()).toBeGreaterThan(0);
    browser.driver.sleep(3000);
  });

 it('should remove from watchlist',async() => { 
    browser.waitForAngular();
    const itemsBeforeRemove = element.all(by.css('.wat-lst-mai-container'));   
    element.all(by.css('.remove')).first().click();   
   
    expect(browser.refresh()).toBeNull();  
    expect(itemsBeforeRemove.count()).toBeLessThan(8);   
     
   
  });

  it('should update the comments',async() => { 
    browser.driver.sleep(5000);
    element.all(by.css('.txtCmt')).first().sendKeys('This id DDLJ KOKO');
    element.all(by.css('.btn-update')).first().click();      
    expect(browser.refresh()).toBeNull();    
   
  });
});
