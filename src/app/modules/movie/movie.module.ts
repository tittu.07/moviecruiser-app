import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThumbnailComponent } from './components/thumbnail/thumbnail.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {MovieService} from './movie.service';
import { ContainerComponent } from './components/container/container.component';
import {MovieRouterModule} from './movie.router.module';
import {MatCardModule} from '@angular/material/card';
import { WatchlistComponent } from './components/watchlist/watchlist.component';
import { TmdbContainerComponent } from './components/tmdb-container/tmdb-container.component'
import { MatButtonModule } from '@angular/material/button';
import{MatSnackBarModule} from '@angular/material/snack-bar';
import {SearchComponent} from './components/Search/search.component'
import { FormsModule } from '@angular/forms';
import { DummyComponent } from './components/Dummy/dummy.component';
import { MovieDetailsComponent } from './components/MovieDetails/moviedetails.component';
import { LoginComponent } from './components/Login/login.component';
import { AuthInterceptor } from './auth.interceptor';
import { RegisterComponent } from './components/register/register.component';



@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    MovieRouterModule,
    MatCardModule,
    MatButtonModule,
    MatSnackBarModule,
    FormsModule

  ],
  declarations: [
   
    ThumbnailComponent,
    ContainerComponent,
    WatchlistComponent,
    TmdbContainerComponent,
    SearchComponent,
    DummyComponent ,
    MovieDetailsComponent,
    LoginComponent,
    RegisterComponent
  ],
  exports:[
    ThumbnailComponent,
    MovieRouterModule
  ],
  providers:[
    MovieService,
    {
      provide:HTTP_INTERCEPTORS,
      useClass:AuthInterceptor,
      multi:true
    }
  ]
})
export class MovieModule { }
