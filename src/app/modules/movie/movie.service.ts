import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map, retry} from 'rxjs/operators';
import {Movie} from './movie';
import {Observable, observable} from 'rxjs';
import {User} from './user'

@Injectable({
  providedIn: 'root'
})
export class MovieService { 
  imagePrefix:string;
  watchlistEndpoint:string;
  movieApi:string;
  bookmarkMovie:string;
  getBookmarkedMovies:string;
  movieSearch:string;
  movieDetails:string;
  deleteBookmarked:string;
  updateMovieurl:string;
  authUrl:string;
  getAllMovies :string;
  loginEndPoint:string;
  registerEndPoint:string;

  constructor(private http:HttpClient) {     
    this.imagePrefix="https://image.tmdb.org/t/p/w500"
    this.movieApi="http://localhost:52467/api/movie/";
    this.authUrl = "http://localhost:52633/api/Auth/";
    //relative urls
    this.getAllMovies="moviesfromtmdb/";
    this.bookmarkMovie="newmovie/";
    this.getBookmarkedMovies = "allmovies/";
    this.movieSearch="search/";
    this.movieDetails="singlemovieinfo/"
    this.deleteBookmarked="movie/";
    this.updateMovieurl = "moviedetails/";
    this.loginEndPoint="login/";
    this.registerEndPoint="register/";
  }
  
  getMovies(type:string,page:number=1):Observable<Array<Movie>>{
    const endpoint = `${this.movieApi}${this.getAllMovies}${type}/${page}`;
    return this.http.get(endpoint).pipe(
      retry(3),
      map(this.pickMovieResults),
      map(this.transformPostrPath.bind(this))
    );    
  }  

  transformPostrPath(movies):Array<Movie>{
   return movies.map(
    movie=>{
      movie.poster_path=`${this.imagePrefix}${movie.poster_path}`
      return movie;
    });    
  }
 

  pickMovieResults(response){
    return response['results'];
  }

  addMoviesToWatchlist(movie){
    const bookmarkUrl = `${this.movieApi}${this.bookmarkMovie}`;
    return this.http.post(bookmarkUrl,movie);
  }

  getWatchlistedMovies():Observable<Array<Movie>>{
    const bookmarkUrl = `${this.movieApi}${this.getBookmarkedMovies}`;
    return this.http.get<Array<Movie>>(bookmarkUrl);
  }
  
  removeWatchlisted(movie){   
    console.log(movie);
    const bookmarkUrl = `${this.movieApi}${this.deleteBookmarked}`;
    return this.http.post(bookmarkUrl,movie);
    
  }
updateMovie(movie){
  const endpoint =`${this.movieApi}${this.updateMovieurl}`; 
  return this.http.post(endpoint,movie);
}

  searchMovies(keyword:string,page:number=1):Observable<Array<Movie>>{
    const endpoint = `${this.movieApi}${this.movieSearch}${keyword}/${page}`;
    return this.http.get<Array<Movie>>(endpoint).pipe(
      retry(3),
      map(this.pickMovieResults),
      map(this.transformPostrPath.bind(this))
    );  
   }

   getMovieInfo(id:number):Observable<Movie>{
    const endpoint = `${this.movieApi}${this.movieDetails}${id}`;    
    return this.http.get<Movie>(endpoint).pipe(
      map(
        
        movie=>{
          var details =JSON.parse( movie["result"]);
          var isWatchListed = movie["isWatchlisted"];
         
          details.poster_path=`${this.imagePrefix}${details.poster_path}`,
          details.comments=movie["comments"],
          details.isWatchListed=isWatchListed

          console.log(details);
        return details;
        })
    );
   }

   authenticateUser(data){
    const endpoint = `${this.authUrl}${this.loginEndPoint}`;    
    return this.http.post(endpoint,data);
   }
   registerUser(data){
    const endpoint = `${this.authUrl}${this.registerEndPoint}`;    
    return this.http.post(endpoint,data);
   }
}
