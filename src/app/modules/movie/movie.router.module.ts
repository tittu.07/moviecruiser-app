import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContainerComponent } from './components/container/container.component';
import {TmdbContainerComponent} from './components/tmdb-container/tmdb-container.component';
import { WatchlistComponent } from './components/watchlist/watchlist.component'
import { SearchComponent } from './components/Search/search.component';
import { DummyComponent } from './components/Dummy/dummy.component';
import { MovieDetailsComponent } from './components/MovieDetails/moviedetails.component';
import { LoginComponent } from './components/Login/login.component';
import { RegisterComponent } from './components/register/register.component';
const movieRoutes: Routes = [
    {
        path: 'movies',
        children: [
            {
                path: '',
                component: LoginComponent               
            },
            {
                path: 'register',
                component: RegisterComponent
            },
            {
                path: 'popular',
                component: TmdbContainerComponent,
                data:{
                    movieType:'popular'
                }
            },
            {
                path: 'top_rated',
                component: TmdbContainerComponent,
                data:{
                    movieType:'top_rated'
                }
            },
            {
                path: 'watchlist',
                component: WatchlistComponent,
               
            },
            {
                path: 'search/:keyword',
                component: SearchComponent,
               
            },   
            {
                path: 'dummy/:keyword',
                component: DummyComponent,
               
            },
            {
                path:'moviedetails/:id',
                component:MovieDetailsComponent
            }                
        ]
    }

];

@NgModule({
imports:[
    RouterModule.forChild(movieRoutes)
],
exports:[
    RouterModule
]
})

export class MovieRouterModule {

}