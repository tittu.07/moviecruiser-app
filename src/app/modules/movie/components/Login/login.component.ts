import { Component, OnInit, Input } from '@angular/core';
import {User} from '../../user'
import { MovieService } from '../../movie.service';
import { Router } from '@angular/router';

@Component({
    selector: 'movie-login', 
    templateUrl:'./login.component.html',
    styleUrls:['./login.component.css'],
    
   
  })

  export class LoginComponent {
  showUname:boolean=false;
  showPwd:boolean=false;
  common:Boolean=false;
error:boolean=false;

    user : User = {
      Id:null,
      FirstName:null,
      Last_Name:null,
      Password:'',
      UserName:''
    };
    token:string;    
     
    constructor(private movieService:MovieService,private router: Router) { 
     
     }
    
     public validateCredentials(){
      this.common=false;
      this.showUname=false;
      this.showPwd=false;
      this.error=false;
       var data = {UserName:this.user.UserName,Password:this.user.Password}
        if(this.user.UserName=='' && this.user.Password==''){
          this.common=true;
        }else if(this.user.UserName==''){
          this.showUname=true;
        }else if(this.user.Password==''){
          this.showPwd=true;
        }else{
          this.movieService.authenticateUser(data).subscribe(
            res=>{  
              localStorage.setItem('token',res['token']);              
              this.router.navigate(['movies','popular']);
             },
             error=>{
               
              this.error=true;
             }
        )
        }
          
        
       
     }
      
     newRegister(){
      this.router.navigate(['movies','register']);
     }

  }