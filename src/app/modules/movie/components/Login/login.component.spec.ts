import{TestBed,async, ComponentFixture,inject} from '@angular/core/testing'
import { LoginComponent } from './login.component';
import { FormsModule } from '../../../../../../node_modules/@angular/forms';
import {HttpClientTestingModule} from '@angular/common/http/testing'
import {RouterTestingModule} from '@angular/router/testing'
import {MovieService} from '../../movie.service'

describe('LoginComponent',()=>{
let fixture : ComponentFixture<LoginComponent>;
let component : LoginComponent;
let service:MovieService;


beforeEach(async(()=>{    

    TestBed.configureTestingModule({
        declarations:[
            LoginComponent,  
                    
            ],
            imports:[                
               FormsModule,
               HttpClientTestingModule,
               RouterTestingModule,
               
            ],
            providers:[
                MovieService,
                            ]
    }).compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance; 

    service = TestBed.get(MovieService) 
}))

it('Logincomponent test file working fine',()=>{
    expect(true).toBe(true);
});

it('Should create the login component',async(()=>{
    const fixture = TestBed.createComponent(LoginComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
}));

it('should same, service and injected one',
inject([MovieService],(injectService:MovieService)=>{
expect(injectService).toBe(service)
}));

it('Should call validation method from service',async(()=>{
    spyOn(service,"authenticateUser").and.returnValue({ subscribe: () => {"hello"} });
    component.user.UserName="test";
    component.user.Password="test";
    component.validateCredentials();
    expect(service.authenticateUser).toHaveBeenCalled();

}));


});