import { Component, OnInit, Input } from '@angular/core';
import {Movie} from '../../movie';
import{MovieService}from '../../movie.service';
import { Router } from '../../../../../../node_modules/@angular/router';


@Component({
  selector: 'movie-watchlist', 
  templateUrl:'./watchlist.component.html',
  styleUrls:['./watchlist.component.css']
 
})
export class WatchlistComponent implements OnInit {
  movies:Array<Movie>; 
  @Input()
  movie:Movie;

  constructor(private movieService:MovieService,private router:Router) { 
    this.movies=[];
  }

  ngOnInit() {
    this.movieService.getWatchlistedMovies().subscribe((movies)=>{
      this.movies.push(...movies);         
    },
    error=>{
      this.router.navigate(['']);   
    })
    
  }

  removeFromWatchlist(index){    
    this.movieService.removeWatchlisted(this.movies[index]).subscribe(data=>{
      window.location.reload();
    });
  }
  
  updateMovieDetails(index){    
    this.movieService.updateMovie(this.movies[index]).subscribe(data=>{
      window.location.reload();
    });
  }

  getMovieDetails(index){
    this.router.navigate(['/movies/moviedetails',this.movies[index].id]);    
  }
  
}
