import{TestBed,async, ComponentFixture,inject} from '@angular/core/testing'
import { WatchlistComponent } from './watchlist.component';
import { FormsModule } from '../../../../../../node_modules/@angular/forms';
import {HttpClientTestingModule} from '@angular/common/http/testing'
import {RouterTestingModule} from '@angular/router/testing'
import {MovieService} from '../../movie.service';
import { MatCardModule } from '../../../../../../node_modules/@angular/material/card';
import { MatSnackBarModule } from '../../../../../../node_modules/@angular/material/snack-bar';



describe('ThumbnailComponent',()=>{
let fixture : ComponentFixture<WatchlistComponent>;
let component : WatchlistComponent;
let service:MovieService;


beforeEach(async(()=>{    

    TestBed.configureTestingModule({
        declarations:[
            WatchlistComponent,  
                    
            ],
            imports:[                
               FormsModule,
               HttpClientTestingModule,
               RouterTestingModule,
               MatCardModule,
               MatSnackBarModule
               
            ],
            providers:[
                MovieService,
                            ]
    }).compileComponents();

    fixture = TestBed.createComponent(WatchlistComponent);
    component = fixture.componentInstance; 
   
    service = TestBed.get(MovieService) 
}))

it('Should call removeWatchlisted',async(()=>{    
    spyOn(service,"removeWatchlisted").and.returnValue({subscribe:()=>{}});
    component.removeFromWatchlist(1);
    expect(service.removeWatchlisted).toHaveBeenCalled();  

}));

it('Should call updateMovie',async(()=>{    
    spyOn(service,"updateMovie").and.returnValue({subscribe:()=>{}});
    component.updateMovieDetails(1);
    expect(service.updateMovie).toHaveBeenCalled();  

}));

it('getMovieDetails shoould truthy',async(()=>{    
    expect(component.getMovieDetails).toBeTruthy();  
}));

});