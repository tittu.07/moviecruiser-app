import { Component, OnInit } from '@angular/core';
import {Movie} from '../../movie' ;
import {MovieService} from '../../movie.service'
import {ActivatedRoute} from '@angular/router'
import { Router } from '@angular/router';

@Component({
  selector: 'movie-movie-details',
  templateUrl:'./moviedetails.component.html',
  styleUrls:['./moviedetails.component.css']
})
export class MovieDetailsComponent implements OnInit {
   movieId:number;
   movie:Movie;
   isWatchlisted:boolean=false;
   buttonName:string="Add to watchlist";
   addStyl:string="addBtnStyl";
   rmvStyl:string="rmvBtnStyl"
   btnStyle:string = this.addStyl;
   update:boolean=false;
   comments:string = "";

    constructor(private routeActive:ActivatedRoute,private movieService:MovieService,
      private router: Router){
        this.routeActive.paramMap.subscribe(
          params => {
             this.movieId = +params.get('id');   
             }   ,
             error=>{
              this.router.navigate(['']);  
             } 
       );      
    }
   

  ngOnInit() {
    this.movieService.getMovieInfo(this.movieId).subscribe(
      data=>{
        this.movie=data; 
        this.isWatchlisted = data["isWatchListed"];  
        if(this.isWatchlisted){
          this.comments=data["comments"];
          this.buttonName = "Remove from watchlist";
          this.btnStyle=this.rmvStyl;
          this.update=true;
        }else{
          this.buttonName ="Add to watchlist";
          this.btnStyle=this.addStyl;
        }         
      },
      error=>{
        console.log("Error")
        console.log(error);
      }
      );             
  }

  addOrRemoveItem(){
    if(this.isWatchlisted){      
      this.removeFromWatchlist();
      // this.btnStyle=this.addStyl;
    }else{
      this.addToWatchlist();
      // this.btnStyle=this.rmvStyl;
    }
  }

removeFromWatchlist(){
  this.movieService.removeWatchlisted(this.movie).subscribe(data=>{
      window.location.reload();
    });
}
  addToWatchlist(){
    this.movie.comments=this.comments;    
    this.movieService.addMoviesToWatchlist(this.movie).subscribe((movie)=>{         
      window.location.reload();
    });
  }

  updateMovie(){
    this.movie.comments=this.comments; 
    this.movieService.updateMovie(this.movie).subscribe(data=>{
      window.location.reload();
    });
  }

  goToHome(){
        this.router.navigate(['movies','popular']);
  }
}
