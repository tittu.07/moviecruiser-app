import{TestBed,async, ComponentFixture,inject} from '@angular/core/testing'
import { MovieDetailsComponent } from './moviedetails.component';
import { FormsModule } from '../../../../../../node_modules/@angular/forms';
import {HttpClientTestingModule} from '@angular/common/http/testing'
import {RouterTestingModule} from '@angular/router/testing'
import {MovieService} from '../../movie.service'


describe('MovieDetailsComponent',()=>{
let fixture : ComponentFixture<MovieDetailsComponent>;
let component : MovieDetailsComponent;
let service:MovieService;


beforeEach(async(()=>{    

    TestBed.configureTestingModule({
        declarations:[
            MovieDetailsComponent,  
                    
            ],
            imports:[                
               FormsModule,
               HttpClientTestingModule,
               RouterTestingModule,
               
            ],
            providers:[
                MovieService,
                            ]
    }).compileComponents();

    fixture = TestBed.createComponent(MovieDetailsComponent);
    component = fixture.componentInstance; 
   
    service = TestBed.get(MovieService) 
}))

it('MovieDetailsComponent test file working fine',()=>{
    expect(true).toBe(true);
});

it('Should create the Movie details component',async(()=>{
    const fixture = TestBed.createComponent(MovieDetailsComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
}));

it('should same, service and injected one',inject([MovieService],(injectService:MovieService)=>{
    expect(injectService).toBe(service)
}));

it('Should call removeFromWatchlist',async(()=>{
    component.isWatchlisted=true;
    spyOn(component,"removeFromWatchlist");
    component.addOrRemoveItem();
    expect(component.removeFromWatchlist).toHaveBeenCalled();  

}));

it('Should call addToWatchlist',async(()=>{
    component.isWatchlisted=false;
    spyOn(component,"addToWatchlist");
    component.addOrRemoveItem();
    expect(component.addToWatchlist).toHaveBeenCalled();  

}));

it('Should call removeWatchlisted method from service',async(()=>{
    spyOn(service,"removeWatchlisted").and.returnValue({ subscribe: () => {} });    
    component.removeFromWatchlist();
    expect(service.removeWatchlisted).toHaveBeenCalled();

}));

it('Should call addToWatchlist method from service',async(()=>{
   
    expect( component.addToWatchlist).toBeTruthy();

}));
it('Should call updateMovie method from service',async(()=>{   
    expect( component.updateMovie).toBeTruthy();
}));


});