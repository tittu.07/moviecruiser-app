import { Component, Input, OnInit } from '@angular/core'; //
import {Movie} from '../../movie';
import{MovieService}from '../../movie.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Route } from '../../../../../../node_modules/@angular/compiler/src/core';

@Component({
  selector: 'movie-search',
  template:`  
  <movie-container [movies]="movies"></movie-container>`
 
})
export class SearchComponent implements OnInit  {//implements OnInit
     keyword:string;
     movies:Array<Movie>;

  constructor(private movieService:MovieService,
    private route :Router,private routeActive:ActivatedRoute) { //private route:ActivatedRoute

    this.movies=[];      

    this.routeActive.paramMap.subscribe(
      params => {
         this.keyword = params.get('keyword'); 
         this.movieService.searchMovies(this.keyword).subscribe((movies)=>{
          this.movies.push(...movies);     
       });             
      }    
   );
   
  
  }

  ngOnInit() {        
     
    
 }

  
}
