import{TestBed,async, ComponentFixture,inject} from '@angular/core/testing'
import { RegisterComponent } from './register.component';
import { FormsModule } from '../../../../../../node_modules/@angular/forms';
import {HttpClientTestingModule} from '@angular/common/http/testing'
import {RouterTestingModule} from '@angular/router/testing'
import {MovieService} from '../../movie.service'

describe('RegisterComponent',()=>{
let fixture : ComponentFixture<RegisterComponent>;
let component : RegisterComponent;
let service:MovieService;


beforeEach(async(()=>{    

    TestBed.configureTestingModule({
        declarations:[
            RegisterComponent,  
                    
            ],
            imports:[                
               FormsModule,
               HttpClientTestingModule,
               RouterTestingModule,
               
            ],
            providers:[
                MovieService,
                            ]
    }).compileComponents();

    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance; 

    service = TestBed.get(MovieService) 
}))

it('Registercomponent test file working fine',()=>{
    expect(true).toBe(true);
});

it('Should create the register component',async(()=>{
    const fixture = TestBed.createComponent(RegisterComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
}));

it('should same, service and injected one',
inject([MovieService],(injectService:MovieService)=>{
expect(injectService).toBe(service)
}));

it('Should call validation method from service',async(()=>{
    spyOn(service,"registerUser").and.returnValue({ subscribe: () => {"hello"} });
    
    component.user.FirstName="asdasd";
    component.user.Last_Name="asdasd";
    component.user.Password="asdasd";
    component.user.UserName="sdfsdf";
    component.register();

    expect(service.registerUser).toHaveBeenCalled();

}));


});