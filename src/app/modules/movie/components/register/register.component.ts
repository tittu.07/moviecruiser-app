import { Component, OnInit, Input } from '@angular/core';
import { MovieService } from '../../movie.service';
import { Router } from '../../../../../../node_modules/@angular/router';
import { User } from '../../user';


@Component({
    selector: 'movie-register', 
    templateUrl:'./register.component.html',
    styleUrls:['./register.component.css'],    
   
  })

 export class RegisterComponent {
  user : User = {
    Id:null,
    FirstName:null,
    Last_Name:null,
    Password:'',
    UserName:''
  };
  showError :boolean = false;
  exist:boolean=false;
  succReg:boolean=false;
    constructor(private movieService:MovieService,private router: Router) {      
    }

    register(){
      this.showError=false;
      this.exist=false;
      this.succReg =false;
      if(this.user.UserName==''||this.user.Password=='' ||this.user.FirstName==''||this.user.Last_Name==''){
        this.showError=true;
      }else{
        var data = {UserName:this.user.UserName,Password:this.user.Password,
                    FirstName:this.user.FirstName,Last_Name:this.user.Last_Name}
        this.movieService.registerUser(data).subscribe(          
            res=>{  
              this.succReg =true;

              setTimeout(() => {
                this.router.navigate(['']); 
              }, (4000));
                          
             }, error=>{
              this.exist=true;       
            })
             
      }
    }

  }