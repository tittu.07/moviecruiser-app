import { Component, Input, OnInit } from '@angular/core'; //
import {Movie} from '../../movie';
import{MovieService}from '../../movie.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Route } from '../../../../../../node_modules/@angular/compiler/src/core';

@Component({
  selector: 'movie-dummy',
  template:`  
  <movie-container [movies]="movies"></movie-container>`
 
})
export class DummyComponent implements OnInit  {   
  keyword:string;
  movies:Array<Movie>;

  
  constructor(private routeActive:ActivatedRoute,private router:Router){
    this.movies=[]; 
    this.routeActive.paramMap.subscribe(
        params => {
           this.keyword = params.get('keyword');                        
        }    
     );
  }

  ngOnInit() {    

    this.router.navigate(['/movies/search',this.keyword]);   
 } 

}
