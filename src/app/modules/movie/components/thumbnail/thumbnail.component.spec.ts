import{TestBed,async, ComponentFixture,inject} from '@angular/core/testing'
import { ThumbnailComponent } from './thumbnail.component';
import { FormsModule } from '../../../../../../node_modules/@angular/forms';
import {HttpClientTestingModule} from '@angular/common/http/testing'
import {RouterTestingModule} from '@angular/router/testing'
import {MovieService} from '../../movie.service';
import { MatCardModule } from '../../../../../../node_modules/@angular/material/card';
import { MatSnackBarModule } from '../../../../../../node_modules/@angular/material/snack-bar';



describe('ThumbnailComponent',()=>{
let fixture : ComponentFixture<ThumbnailComponent>;
let component : ThumbnailComponent;
let service:MovieService;


beforeEach(async(()=>{    

    TestBed.configureTestingModule({
        declarations:[
            ThumbnailComponent,  
                    
            ],
            imports:[                
               FormsModule,
               HttpClientTestingModule,
               RouterTestingModule,
               MatCardModule,
               MatSnackBarModule
               
            ],
            providers:[
                MovieService,
                            ]
    }).compileComponents();

    fixture = TestBed.createComponent(ThumbnailComponent);
    component = fixture.componentInstance; 
   
    service = TestBed.get(MovieService) 
}))

it('ThumbnailComponent test file working fine',()=>{
    expect(true).toBe(true);
});

it('Should create the thumbnail component',async(()=>{
    const fixture = TestBed.createComponent(ThumbnailComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
}));

it('should same, service and injected one',inject([MovieService],(injectService:MovieService)=>{
    expect(injectService).toBe(service)
}));

it('Should call removeWatchlisted',async(()=>{    
    spyOn(component,"removeWatchlisted");
    component.removeWatchlisted();
    expect(component.removeWatchlisted).toHaveBeenCalled();  

}));

it('getMovieDetails should be truthy',async(()=>{    
    
    expect(component.getMovieDetails).toBeTruthy();  

}));



});