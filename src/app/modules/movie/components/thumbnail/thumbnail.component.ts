import { Component, OnInit, Input } from '@angular/core';
import {Movie} from '../../movie' ;
import {MovieService} from '../../movie.service'
import {MatSnackBar} from '@angular/material/snack-bar';
import { Router } from '../../../../../../node_modules/@angular/router';

@Component({
  selector: 'movie-thumbnail',
  templateUrl: './thumbnail.component.html',
  styleUrls: ['./thumbnail.component.css']
})
export class ThumbnailComponent implements OnInit {
  @Input()
  movie:Movie;
 
  constructor(private movieService:MovieService,private snackBar :MatSnackBar,
    private router: Router) {
    
     }

  ngOnInit() {
  }

 
  
  getMovieDetails(){
    this.router.navigate(['/movies/moviedetails',this.movie.id]);    
  }

  removeWatchlisted(){
   this.movieService.removeWatchlisted(this.movie).subscribe(data=>{
     window.location.reload();
   });

   
  }

}
