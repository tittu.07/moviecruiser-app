import { TestBed,inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { Movie } from './movie';
import { MovieService } from './movie.service';




describe('MovieServiceTest',()=>{
    let service = MovieService;
    let httpMock: HttpTestingController;

    beforeEach(()=>{  
        TestBed.configureTestingModule({
            declarations:[],
                imports:[HttpClientTestingModule ],
                providers:[
                    MovieService,
                                ]
        }).compileComponents();

    service = TestBed.get(MovieService) ;
    httpMock=TestBed.get(HttpTestingController);
})

afterEach(()=>{
  //  httpMock.verify();
})

it('MovieService should be created',inject([MovieService],(mockService:MovieService)=>{
    expect(true).toBe(true);
}));


it('To test the getMovies method call via GET request',inject([MovieService],(mockService:MovieService)=>{
    const movies:Array<Movie> = [
        {comments:'fgh',isWatchlisted:true,id:1,overview:"fgh",poster_path:"",released_date:"",title:"ert"},
        {comments:'fgh',isWatchlisted:true,id:2,overview:"ert",poster_path:"",released_date:"",title:"dfg"}
    ];
    mockService.getMovies("popular",1).subscribe(x=>{
        expect(x.length).toBe(2);
        const request =httpMock.expectOne(`${mockService.movieApi}${mockService.getAllMovies}popular/1`);
        expect(request.request.method).toBe('GET');
        request.flush(movies);
    })
}));

it('Test addMoviesToWatchlist using POST method',inject([MovieService],(mockService:MovieService)=>{
    const movie:Movie = {comments:'fgh',isWatchlisted:true,id:1,overview:"fgh",poster_path:"",released_date:"",title:"ert"};
    mockService.addMoviesToWatchlist(movie).subscribe(x=>{
        const request =httpMock.expectOne(`${mockService.movieApi}${mockService.bookmarkMovie}`);
        expect(request.request.method).toBe('POST');
        request.flush(movie);
    })
}));

it('Test getWatchlistedMovies using get method',inject([MovieService],(mockService:MovieService)=>{
    mockService.getWatchlistedMovies().subscribe(x=>{
        const request =httpMock.expectOne(`${mockService.movieApi}${mockService.getBookmarkedMovies}`);
        expect(request.request.method).toBe('POST');
        request.flush([]);
    })
}));

it('Test removeWatchlisted using get method',inject([MovieService],(mockService:MovieService)=>{
    const movie:Movie = {comments:'fgh',isWatchlisted:true,id:1,overview:"fgh",poster_path:"",released_date:"",title:"ert"};
    mockService.removeWatchlisted(movie).subscribe(x=>{
        const request =httpMock.expectOne(`${this.movieApi}${this.getBookmarkedMovies}`);
        expect(request.request.method).toBe('GET');
        request.flush([]);
    })
}));

it('Test updateMovie using get method',inject([MovieService],(mockService:MovieService)=>{
    const movie:Movie = {comments:'fgh',isWatchlisted:true,id:1,overview:"fgh",poster_path:"",released_date:"",title:"ert"};
    mockService.updateMovie(movie).subscribe(x=>{
        const request =httpMock.expectOne(`${this.movieApi}${this.getBookmarkedMovies}`);
        expect(request.request.method).toBe('POST');
        request.flush([]);
    })
}));

it('Test searchMovies using get method',inject([MovieService],(mockService:MovieService)=>{
    //const movie:Movie = {comments:'fgh',isWatchlisted:true,id:1,overview:"fgh",poster_path:"",released_date:"",title:"ert"};
    mockService.searchMovies('Test',1).subscribe(x=>{
        const request =httpMock.expectOne(`${this.movieApi}${this.getBookmarkedMovies}`);
        expect(request.request.method).toBe('GET');
        request.flush([]);
    })
}));

it('Test getMovieInfo using get method',inject([MovieService],(mockService:MovieService)=>{
    //const movie:Movie = {comments:'fgh',isWatchlisted:true,id:1,overview:"fgh",poster_path:"",released_date:"",title:"ert"};
    mockService.getMovieInfo(1).subscribe(x=>{
        const request =httpMock.expectOne(`${this.movieApi}${this.getBookmarkedMovies}`);
        expect(request.request.method).toBe('GET');
        request.flush([]);
    })
}));

});

