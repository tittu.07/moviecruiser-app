import { TestBed } from '@angular/core/testing';
import {  HttpClientTestingModule,  HttpTestingController,} from '@angular/common/http/testing';
import { MovieService } from './movie.service';
import { AuthInterceptor } from './auth.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

describe(`AuthInterceptor`, () => {
  let service: MovieService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        MovieService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptor,
          multi: true,
        },
      ],
    });

    service = TestBed.get(MovieService);
    httpMock = TestBed.get(HttpTestingController);
  });



  it('should add an Authorization header', () => {
    service.getMovies("test").subscribe(response => {
      expect(response).toBeTruthy();
    });
  
    const httpRequest = httpMock.expectOne(`${service.movieApi}${service.getAllMovies}test/1`);
  
    expect(httpRequest.request.headers.has('Authorization'));
  });

});