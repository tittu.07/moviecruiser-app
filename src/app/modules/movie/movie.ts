export interface Movie{
    id:number;
    title:string;
    poster_path:string;
    overview: string;
    released_date:string;  
    isWatchlisted:boolean;
    comments:string;
};