import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
 templateUrl:'./app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private router: Router){

  }
  title = 'app';

 @Input()
 keyword:string='';

 goToSearch(){
   if(this.keyword.length>0){
    this.router.navigate(['/movies/dummy',this.keyword]);
    this.keyword="";
   }  
 }

  
}
