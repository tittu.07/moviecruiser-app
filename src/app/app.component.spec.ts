import {TestBed,async, ComponentFixture} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {Routes, RouterModule} from '@angular/router';
import {MatToolbarModule} from '@angular/material/toolbar';
import { FormsModule } from '../../node_modules/@angular/forms';
import {APP_BASE_HREF} from '@angular/common';
import { ExpectedConditions } from '../../node_modules/protractor';
import { ExpressionType } from '../../node_modules/@angular/compiler';
import { AppModule } from './app.module';

const appRoutes:Routes=[{
    path:'',
    redirectTo:'movies',
    pathMatch:'full'
    }]

describe('AppComponent',()=>{
    let fixture : ComponentFixture<AppComponent>;
    let component : AppComponent;
    beforeEach(async(()=>{
        TestBed.configureTestingModule({
            declarations:[
            AppComponent,           
            ],
            providers:[
                {provide: APP_BASE_HREF, useValue : '/' }
            ],
            imports:[
                RouterModule.forRoot(appRoutes),
                MatToolbarModule,
                FormsModule,
            ]
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance; 
}));

it('Appcomponent test file working fine',()=>{
    expect(true).toBe(true);
});

it('Should create the app',async(()=>{
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
}));

it('goToSearch method should be truthy',async(()=>{
    const fixture1 = TestBed.createComponent(AppComponent);
    const app = fixture1.debugElement.componentInstance;
    expect(component.goToSearch).toBeTruthy();
}));


});