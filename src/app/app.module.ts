import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import {MovieModule} from './modules/movie/movie.module'
import { AppComponent } from './app.component';
import {RouterModule,Routes} from '@angular/router'
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
import {MatToolbarModule} from '@angular/material/toolbar'
import {MatButtonModule} from '@angular/material/button'

const appRoutes:Routes=[{
path:'',
redirectTo:'movies',
pathMatch:'full'
}]
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    MovieModule,
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
